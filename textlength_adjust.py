#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Text Length Adjust - Basic Text Length attribute changes
##############################################################################

import inkex

class textlengthAdjust(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--textlength_adjust_notebook", type=str, dest="textlength_adjust_notebook_page", default=0)

        pars.add_argument("--textlength_adjust", type=float, dest="textlength_adjust", default=0)

        pars.add_argument("--length_adjust_radio", type=str, dest="length_adjust_radio", default='spacing')

        pars.add_argument("--textlength_remove_checkbox", type=str, dest="textlength_remove_checkbox")

    def effect(self):

        my_objects = self.svg.selected
        if len(my_objects) < 1:
            inkex.errormsg('Please Select A Single Text Object')
            return
        my_object = self.svg.selected[0]

        if my_object.TAG != 'text':
            inkex.errormsg(f'Please Select A Single Text Object \n This Object is a {my_object.TAG}')


        if self.options.textlength_remove_checkbox == 'true':
            my_object.attrib['lengthAdjust'] = 'none'
            my_object.attrib['textLength'] = 'none'

            del my_object.attrib['lengthAdjust']
            del my_object.attrib['textLength']

            return

        my_textlength = self.options.textlength_adjust

        if self.options.length_adjust_radio == 'spacing':
            my_length_adjust = 'spacing'
        else:
            my_length_adjust = 'spacingAndGlyphs'

        my_object.attrib['lengthAdjust'] = str(my_length_adjust)
        my_object.attrib['textLength'] = str(my_textlength)

if __name__ == '__main__':
    textlengthAdjust().run()
