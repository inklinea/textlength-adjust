# Textlength Adjust

Inkscape Extension - Simple textLength adjustment

Inkscape 1.1+

Appears under Extensions>Text

▶ Simple 'textLength' attribute adjustment
